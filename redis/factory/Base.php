<?php
namespace lock\redis\factory;

class Base
{
    /**
     * @var object redis对象
     */
    protected $redis;
    public function __construct($redis){
        $this->redis = $redis ;
    }
}